package com.ecommerce.shoppingcart.repository;

import org.springframework.data.repository.CrudRepository;

import com.ecommerce.shoppingcart.domain.Account;
import com.ecommerce.shoppingcart.domain.Product;

public interface AccountRepository extends CrudRepository<Account, String>{
	Account findByUsername(String username);
}
