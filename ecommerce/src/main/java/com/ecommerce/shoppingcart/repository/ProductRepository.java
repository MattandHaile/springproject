package com.ecommerce.shoppingcart.repository;

import org.springframework.data.repository.CrudRepository;

import com.ecommerce.shoppingcart.domain.Product;

public interface ProductRepository extends CrudRepository<Product, Integer>{

}
