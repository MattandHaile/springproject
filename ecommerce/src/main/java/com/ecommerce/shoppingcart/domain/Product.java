package com.ecommerce.shoppingcart.domain;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.type.ImageType;

@Entity
@Table(name = "product")
public class Product implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	
	private String name;
	
	public String photo;
	
	private double price;
	
	
	private String buy;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getBuy() {
		return buy;
	}
	public void setBuy(String nuy) {
		this.buy = buy;
	}
	public Product(int id, String name, double price, String buy) {
		super();
		this.id = id;
		this.name = name;
		this.price = price;
		this.buy = buy;
	}
	public Product() {
		super();
	}
	

}
