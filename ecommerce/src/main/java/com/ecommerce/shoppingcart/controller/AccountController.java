package com.ecommerce.shoppingcart.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;

import com.ecommerce.shoppingcart.domain.Account;
import com.ecommerce.shoppingcart.service.AccountService;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class AccountController {

	@Autowired
	private AccountService accountService;
	
	@GetMapping("/account") 
	public String Login() {
		return "account/index";
	}
	
	@ModelAttribute("account")
	public Account account() {
		return new Account();
	}
	
	@GetMapping("/signup") 
	public String signUp() {
		return "account/signup";
	}
	
	@PostMapping("/signup")
	public String signup(@Valid @ModelAttribute("account") Account account,Errors error) {
		if(error.hasErrors()) {
			System.out.print("error" + error + "Acccount " + account);
			return "account/signup";
		}
		account.setPassword(account.getPassword());
		accountService.create(account);
		return "redirect:../account";
	}
}
