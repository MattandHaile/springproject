package com.ecommerce.shoppingcart.service;



import com.ecommerce.shoppingcart.domain.Product;



public interface ProductService {
	
	public Iterable<Product> findAll();
	public Product find(int id);
}
