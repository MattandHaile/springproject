package com.ecommerce.shoppingcart.service;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;

import com.ecommerce.shoppingcart.domain.Account;
import com.ecommerce.shoppingcart.domain.Product;
import com.ecommerce.shoppingcart.repository.AccountRepository;

@Service
public class AccountServiceImpl implements AccountService{

	
	@Autowired
	private AccountRepository accountRepo;
	
	@Override
	public void create(Account account) {
		accountRepo.save(account);
	}
	

	@Override
	public Account find(String username) {
		return accountRepo.findByUsername(username);
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Account account = this.find(username);		
		if(account != null) {
				return account;
		}			
		throw new UsernameNotFoundException("user not found");
	}
	
	
}
