package com.ecommerce.shoppingcart;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.ecommerce.shoppingcart.controller.AccountController;
import com.ecommerce.shoppingcart.service.AccountService;

@RunWith(SpringJUnit4ClassRunner.class)
@WebMvcTest(AccountController.class)
public class AcciubtControllerTest {

		@Autowired
		private MockMvc mockMvc;
		
		@Autowired
		private AccountController accountController;
		
		@Autowired
		private AccountService accountService;
		
		@Test
		public void testLogin() throws Exception {
			mockMvc.perform(get("/account"))
			.andExpect(status().isOk())
			.andExpect(view().name("index"));
		
		}
		
}
