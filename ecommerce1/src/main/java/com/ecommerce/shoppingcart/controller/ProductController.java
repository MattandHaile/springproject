package com.ecommerce.shoppingcart.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ecommerce.shoppingcart.domain.Product;

import com.ecommerce.shoppingcart.service.ProductService;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
@Controller
@RequestMapping()
public class ProductController {
	
	@Autowired
	private ProductService productService;
	
	
	@GetMapping
    public String products(Model model){
		Iterable<Product> products=productService.findAll(); 
		model.addAttribute("products",products);
        return "product/index";
    }
	

	
}
