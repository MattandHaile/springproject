package com.ecommerce.shoppingcart.service;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ecommerce.shoppingcart.domain.Product;
import com.ecommerce.shoppingcart.repository.ProductRepository;

@Service("ProductService")
@Transactional
public class ProductServiceImpl implements ProductService{
	
	@Autowired
	private ProductRepository productRepo;
	
	@Override
	public Iterable<Product> findAll(){
		return productRepo.findAll();
	}
	
	@Override
	public Product find(int id) {
		Optional<Product> optPro = productRepo.findById(id);
		return optPro.get();
	}
	
	

}
