package com.ecommerce.shoppingcart.service;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.ecommerce.shoppingcart.domain.Account;

public interface AccountService extends UserDetailsService{

	public void create(Account account);
	public Account find(String username);
	
}
