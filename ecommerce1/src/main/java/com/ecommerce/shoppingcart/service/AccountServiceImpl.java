package com.ecommerce.shoppingcart.service;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;

import com.ecommerce.shoppingcart.domain.Account;
import com.ecommerce.shoppingcart.domain.Product;
import com.ecommerce.shoppingcart.repository.AccountRepository;

import lombok.extern.slf4j.Slf4j;
@Slf4j
@Service
public class AccountServiceImpl implements AccountService{

	
	@Autowired
	private AccountRepository accountRepo;
	
	@Override
	public void create(Account account) {
		accountRepo.save(account);
	}
	

	@Override
	public Account find(String username) {
		return accountRepo.findByEmail(username);
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		log.info(username);
		UserDetails account = this.find(username);
		log.info(account.getUsername()+ "UserName");
		if(account != null) {
			log.info(account.getUsername()+ " UserName2");
			
			return account;
		}	
		log.info(account.getUsername()+ " UserName3");
		
		throw new UsernameNotFoundException("user not found");
	}
	
	
}
