package com.ecommerce.shoppingcart.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name = "item")
public class Item implements Serializable{

	
	private Product product;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int quantity;
	 
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	public Item() {
		super();
	}
	public Item(Product product, int quantity) {
		super();
		this.product = product;
		this.quantity = quantity;
	}
}

